package se.lexicon.sina.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import se.lexicon.sina.entity.LibraryUser;

import java.util.Optional;

public interface LibraryUserRepository extends PagingAndSortingRepository<LibraryUser , Integer> {
    Optional<LibraryUser> findByEmail(String email);
}
