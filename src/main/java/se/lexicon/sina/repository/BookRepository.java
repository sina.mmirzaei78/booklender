package se.lexicon.sina.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import se.lexicon.sina.entity.Book;
import se.lexicon.sina.entity.Loan;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends PagingAndSortingRepository<Book, Integer> {
    List<Book> findAllByAvailable(boolean available);
    List<Book> findAllByReserved(boolean reserved);
    List<Book> findAllByTitle(String title);
}
