package se.lexicon.sina.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import se.lexicon.sina.entity.Loan;

import java.util.List;

public interface LoanRepository extends PagingAndSortingRepository<Loan , Long> {
    List<Loan> findByLibraryUser_UserId(int userId);
    List<Loan> findByBook_BookId(int bookId);
    List<Loan> findAllByisTerminated(boolean isTerminated);
}
