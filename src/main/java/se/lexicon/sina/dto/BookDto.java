package se.lexicon.sina.dto;

import se.lexicon.sina.entity.Loan;

import java.util.List;

public class BookDto {

    private int bookId;
    private String title;
    private boolean available;
    private  boolean reserved;
    private  int maxLoanDays;
    private long finePerDay;
    private String description;
    private List<LoanDto> loanDtos;

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }

    public int getMaxLoanDays() {
        return maxLoanDays;
    }

    public void setMaxLoanDays(int maxLoanDays) {
        this.maxLoanDays = maxLoanDays;
    }

    public long getFinePerDay() {
        return finePerDay;
    }

    public void setFinePerDay(long finePerDay) {
        this.finePerDay = finePerDay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<LoanDto> getLoanDtos() {
        return loanDtos;
    }

    public void setLoanDtos(List<LoanDto> loanDtos) {
        this.loanDtos = loanDtos;
    }
}
