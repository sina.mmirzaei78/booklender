package se.lexicon.sina.dto;

import se.lexicon.sina.entity.Book;
import se.lexicon.sina.entity.LibraryUser;

import java.time.LocalDate;

public class LoanDto {

    private long loanId;
    private LocalDate loanDate;
    private boolean isTerminated;

    private BookDto bookDto;
    private LibraryUserDto libraryUserDto;

    public long getLoanId() {
        return loanId;
    }

    public void setLoanId(long loanId) {
        this.loanId = loanId;
    }

    public LocalDate getLoanDate() {
        return loanDate;
    }

    public void setLoanDate(LocalDate loanDate) {
        this.loanDate = loanDate;
    }

    public boolean isisTerminated() {
        return isTerminated;
    }

    public void setisTerminated(boolean isTerminated) {
        this.isTerminated = isTerminated;
    }

    public BookDto getBookDto() {
        return bookDto;
    }

    public void setBookDto(BookDto bookDto) {
        this.bookDto = bookDto;
    }

    public LibraryUserDto getLibraryUserDto() {
        return libraryUserDto;
    }

    public void setLibraryUserDto(LibraryUserDto libraryUserDto) {
        this.libraryUserDto = libraryUserDto;
    }
}
