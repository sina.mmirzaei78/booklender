package se.lexicon.sina.dto;

import se.lexicon.sina.entity.Loan;

import java.time.LocalDate;
import java.util.List;

public class LibraryUserDto {

    private int userId;
    private LocalDate regDate;
    private String name;
    private String email;
    private List<LoanDto> loanDtos;

    public List<LoanDto> getLoanDtos() {
        return loanDtos;
    }

    public void setLoanDtos(List<LoanDto> loanDtos) {
        this.loanDtos = loanDtos;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public LocalDate getRegDate() {
        return regDate;
    }

    public void setRegDate(LocalDate regDate) {
        this.regDate = regDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}
