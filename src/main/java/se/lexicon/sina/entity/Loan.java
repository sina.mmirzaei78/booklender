package se.lexicon.sina.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name="loan")
public class Loan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long loanId;
    private LocalDate loanDate;
    private boolean isTerminated;

    @ManyToOne
    @JoinColumn(name = "bookId")
    @JsonIgnore
    private Book book;

    @ManyToOne
    @JoinColumn(name = "userId")
    @JsonIgnore
    private LibraryUser libraryUser;

    public long getLoanId() {
        return loanId;
    }

    public void setLoanId(long loanId) {
        this.loanId = loanId;
    }

    public LocalDate getLoanDate() {
        return loanDate;
    }

    public void setLoanDate(LocalDate loanDate) {
        this.loanDate = loanDate;
    }

    public boolean isisTerminated() {
        return isTerminated;
    }

    public void setisTerminated(boolean isTerminated) {
        this.isTerminated = isTerminated;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public LibraryUser getLibraryUser() {
        return libraryUser;
    }

    public void setLibraryUser(LibraryUser libraryUser) {
        this.libraryUser = libraryUser;
    }

    @Override
    public String toString() {
        return "Loan{" +
                "loanId=" + loanId +
                ", loanDate=" + loanDate +
                ", isTerminated=" + isTerminated +
                ", book=" + book +
                ", libraryUser=" + libraryUser +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Loan loan = (Loan) o;
        return loanId == loan.loanId &&
                loanDate == loan.loanDate &&
                isTerminated == loan.isTerminated &&
                Objects.equals(book, loan.book) &&
                Objects.equals(libraryUser, loan.libraryUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(loanId, loanDate, isTerminated, book, libraryUser);
    }
}
