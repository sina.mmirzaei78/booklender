package se.lexicon.sina.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.lexicon.sina.dto.LibraryUserDto;
import se.lexicon.sina.entity.LibraryUser;
import se.lexicon.sina.repository.LibraryUserRepository;
import se.lexicon.sina.service.LibraryUserService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class LibraryUserImpl implements LibraryUserService {

    @Autowired
    private LibraryUserRepository libraryUserRepository;

    @Override
    public LibraryUserDto findById(int userId) {
        Optional<LibraryUser> libraryUser = libraryUserRepository.findById(userId);
        if (!libraryUser.isPresent()){
            return null;
        }
        return new ModelMapper().map(libraryUser.get(),LibraryUserDto.class);
    }

    @Override
    public LibraryUserDto findByEmail(String email) {
        Optional<LibraryUser> libraryUser = libraryUserRepository.findByEmail(email);
        if (!libraryUser.isPresent()){
            return null;
        }
        ModelMapper modelMapper = new ModelMapper();
        LibraryUserDto libraryUserDto = modelMapper.map(libraryUser.get(), LibraryUserDto.class);
        return libraryUserDto;
    }

    @Override
    public List<LibraryUserDto> findAll() {
        List<LibraryUser> libraryUsers = (List<LibraryUser>) libraryUserRepository.findAll();
        if (libraryUsers.size() == 0) {
            return null;
        }
        List<LibraryUserDto> libraryUserDtos = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();
        for (int i = 0; i < libraryUsers.size(); i++) {
            LibraryUserDto libraryUserDto = modelMapper.map(libraryUsers.get(i), LibraryUserDto.class);
            libraryUserDtos.add(libraryUserDto);
        }
        return libraryUserDtos;
    }

    @Override
    public LibraryUserDto create(LibraryUserDto libraryUserDto) {
        libraryUserDto.setRegDate(LocalDate.now());
        ModelMapper modelMapper = new ModelMapper();
        LibraryUser libraryUser = modelMapper.map(libraryUserDto, LibraryUser.class);
        libraryUserRepository.save(libraryUser);
        return libraryUserDto;
    }

    @Override
    public LibraryUserDto update(LibraryUserDto libraryUserDto) {
        ModelMapper modelMapper = new ModelMapper();
        LibraryUser libraryUser = modelMapper.map(libraryUserDto, LibraryUser.class);
        libraryUserRepository.save(libraryUser);
        return libraryUserDto;
    }

    @Override
    public boolean delete(int userId) {
        libraryUserRepository.deleteById(userId);
        return true;
    }
}
