package se.lexicon.sina.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.lexicon.sina.dto.BookDto;
import se.lexicon.sina.dto.LoanDto;
import se.lexicon.sina.entity.Loan;
import se.lexicon.sina.repository.LoanRepository;
import se.lexicon.sina.service.LoanService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LoanServiceImpl implements LoanService {

    @Autowired
    private LoanRepository loanRepository;

    @Override
    public LoanDto findById(long loanId) {
        Optional<Loan> loan = loanRepository.findById(loanId);
        if (!loan.isPresent()){
            return null;
        }
        return new ModelMapper().map(loan.get(),LoanDto.class);
    }

    @Override
    public List<LoanDto> findByBookId(int bookId) {
        List<Loan> loans = loanRepository.findByBook_BookId(bookId);
        if (loans.size() == 0){
            return null;
        }
        List<LoanDto> loanDtos = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();
        for (int i = 0; i < loans.size(); i++) {
            LoanDto loanDto = modelMapper.map(loans.get(i), LoanDto.class);
            loanDtos.add(loanDto);
        }
        return loanDtos;
    }

    @Override
    public List<LoanDto> findByUserId(int userId) {
        List<Loan> loans = loanRepository.findByLibraryUser_UserId(userId);
        if (loans.size() == 0){
            return null;
        }
        List<LoanDto> loanDtos = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();
        for (int i = 0; i < loans.size(); i++) {
            LoanDto loanDto = modelMapper.map(loans.get(i), LoanDto.class);
            loanDtos.add(loanDto);
        }
        return loanDtos;
    }

    @Override
    public List<LoanDto> findByisTerminated(boolean isisTerminated) {
        List<Loan> loans = loanRepository.findAllByisTerminated(isisTerminated);
        if (loans.size() == 0) {
            return null;
        }
        List<LoanDto> loanDtos = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();
        for (int i = 0; i < loans.size(); i++) {
            LoanDto loanDto = modelMapper.map(loans.get(i), LoanDto.class);
            loanDtos.add(loanDto);
        }
        return loanDtos;
    }

    @Override
    public List<LoanDto> findAll() {
        List<Loan> loans = (List<Loan>) loanRepository.findAll();
        if (loans.size() == 0) {
            return null;
        }
        List<LoanDto> loanDtos = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();
        for (int i = 0; i < loans.size(); i++) {
            LoanDto loanDto = modelMapper.map(loans.get(i), LoanDto.class);
            loanDtos.add(loanDto);
        }
        return loanDtos;
    }

    @Override
    public LoanDto create(LoanDto loanDto) {
        loanDto.setLoanDate(LocalDate.now());
        ModelMapper modelMapper = new ModelMapper();
        Loan loan = modelMapper.map(loanDto, Loan.class);
        loanRepository.save(loan);
        return loanDto;
    }

    @Override
    public LoanDto update(LoanDto loanDto) {
        ModelMapper modelMapper = new ModelMapper();
        Loan loan = modelMapper.map(loanDto, Loan.class);
        loanRepository.save(loan);
        return loanDto;
    }

}
