package se.lexicon.sina.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.lexicon.sina.dto.BookDto;
import se.lexicon.sina.entity.Book;
import se.lexicon.sina.repository.BookRepository;
import se.lexicon.sina.service.BookService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public List<BookDto> findAllByReserved(boolean isReserved) {
        List<Book> books = bookRepository.findAllByReserved(isReserved);
        if (books.size() == 0) {
            return null;
        }
        List<BookDto> bookDtos = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();
        for (int i = 0; i < books.size(); i++) {
            BookDto bookDto = modelMapper.map(books.get(i), BookDto.class);
            bookDtos.add(bookDto);
        }
        return bookDtos;
    }

    @Override
    public List<BookDto> findAllByAvailable(boolean isAvailable) {
        List<Book> books = bookRepository.findAllByAvailable(isAvailable);
        if (books.size() == 0) {
            return null;
        }
        List<BookDto> bookDtos = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();
        for (int i = 0; i < books.size(); i++) {
            BookDto bookDto = modelMapper.map(books.get(i), BookDto.class);
            bookDtos.add(bookDto);
        }
        return bookDtos;
    }

    @Override
    public List<BookDto> findAllByTitle(String title) {
        List<Book> books = bookRepository.findAllByTitle(title);
        if (books.size() == 0) {
            return null;
        }
        List<BookDto> bookDtos = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();
        for (int i = 0; i < books.size(); i++) {
            BookDto bookDto = modelMapper.map(books.get(i), BookDto.class);
            bookDtos.add(bookDto);
        }
        return bookDtos;
    }


    @Override
    public BookDto findById(int bookId) {
        Optional<Book> book = bookRepository.findById(bookId);
        if (!book.isPresent()) {
            return null;
        }
        ModelMapper modelMapper = new ModelMapper();
        BookDto bookDto = modelMapper.map(book.get(), BookDto.class);
        return bookDto;
    }

    @Override
    public List<BookDto> findAll() {
        List<Book> books = (List<Book>) bookRepository.findAll();
        if (books.size() == 0) {
            return null;
        }
        List<BookDto> bookDtos = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();
        for (int i = 0; i < books.size(); i++) {
            BookDto bookDto = modelMapper.map(books.get(i), BookDto.class);
            bookDtos.add(bookDto);
        }
        return bookDtos;
    }

    @Override
    public BookDto create(BookDto bookDto) {
        ModelMapper modelMapper = new ModelMapper();
        Book book = modelMapper.map(bookDto, Book.class);
        bookRepository.save(book);
        return bookDto;
    }

    @Override
    public BookDto update(BookDto bookDto) {
        ModelMapper modelMapper = new ModelMapper();
        Book book = modelMapper.map(bookDto, Book.class);
        bookRepository.save(book);
        return bookDto;
    }

    @Override
    public boolean delete(int bookId) {
        bookRepository.deleteById(bookId);
        return true;
    }
}
