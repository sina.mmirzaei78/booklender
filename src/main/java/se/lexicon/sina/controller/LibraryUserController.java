package se.lexicon.sina.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.lexicon.sina.dto.BookDto;
import se.lexicon.sina.dto.LibraryUserDto;
import se.lexicon.sina.service.LibraryUserService;

import java.util.List;

@RestController
public class LibraryUserController {

    @Autowired
    private LibraryUserService libraryUserService;

    @RequestMapping(value = "/libraryUser/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity create(@RequestBody LibraryUserDto libraryUserDto) {
        try {
            LibraryUserDto userDto = libraryUserService.findByEmail(libraryUserDto.getEmail());
            if (userDto == null) {
                return new ResponseEntity<>(libraryUserService.create(libraryUserDto), HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/libraryUser/findByEmail/{email}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity findUserByEmail(@PathVariable("email") String email) {
        try {
            LibraryUserDto libraryUserDto = libraryUserService.findByEmail(email);
            if (libraryUserDto != null){
                return new ResponseEntity<>(libraryUserDto , HttpStatus.FOUND);
            }
            else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/libraryUser/find/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity find(@PathVariable("userId") int userId) {
        try {
            LibraryUserDto libraryUserDto = libraryUserService.findById(userId);
            if (libraryUserDto != null) {
                return new ResponseEntity<>(libraryUserDto , HttpStatus.FOUND);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/libraryUser/findAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity findAll() {
        try {
            List<LibraryUserDto> libraryUserDtos = libraryUserService.findAll();
            if (libraryUserDtos != null) {
                return new ResponseEntity<>(libraryUserDtos , HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/libraryUser/delete/{userId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity delete(@PathVariable("userId") int userId) {
        try {
            LibraryUserDto libraryUserDto = libraryUserService.findById(userId);
            if (libraryUserDto != null) {
                libraryUserService.delete(userId);
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/libraryUser/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity update(@RequestBody LibraryUserDto libraryUserDto) {
        try {
            LibraryUserDto currentLibraryUserDto = libraryUserService.findById(libraryUserDto.getUserId());
            if (currentLibraryUserDto != null) {
                LibraryUserDto user = libraryUserService.findByEmail(libraryUserDto.getEmail());
                if (user == null){
                    libraryUserService.update(libraryUserDto);
                    return new ResponseEntity<>(HttpStatus.OK);
                }
                else {
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }

            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
