package se.lexicon.sina.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.lexicon.sina.dto.BookDto;
import se.lexicon.sina.dto.LibraryUserDto;
import se.lexicon.sina.dto.LoanDto;
import se.lexicon.sina.entity.Loan;
import se.lexicon.sina.service.BookService;
import se.lexicon.sina.service.LibraryUserService;
import se.lexicon.sina.service.LoanService;

@RestController
public class LoanController {

    @Autowired
    private LoanService loanService;
    @Autowired
    private BookService bookService;
    @Autowired
    private LibraryUserService libraryUserService;

    @RequestMapping(value = "/loan/add/{userId}/{bookId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity create(@RequestBody LoanDto loanDto, @PathVariable("userId") int userId, @PathVariable("bookId") int bookId) {
        try {
            BookDto bookDto = bookService.findById(bookId);
            LibraryUserDto libraryUserDto = libraryUserService.findById(userId);
            if (bookDto != null && libraryUserDto != null && bookDto.isReserved() == false) {

                loanDto.setBookDto(bookDto);
                loanDto.setLibraryUserDto(libraryUserDto);
                loanService.create(loanDto);

                //changing the reserve to true
                bookDto.setReserved(true);
                bookDto.setBookId(bookId);
                bookService.update(bookDto);

                return new ResponseEntity<>(HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/loan/find/{loanId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity find(@PathVariable("loanId") long loanId) {
        try {
            LoanDto loanDto = loanService.findById(loanId);
            if (loanDto != null) {
                return new ResponseEntity<>(loanDto , HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }


    @RequestMapping(value = "/loan/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity update(@RequestBody LoanDto loanDto) {
        try {
            LoanDto currentLoanDto = loanService.findById(loanDto.getLoanId());
            if (currentLoanDto != null) {
                loanService.update(loanDto);
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
