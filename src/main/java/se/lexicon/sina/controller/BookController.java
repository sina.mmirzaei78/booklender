package se.lexicon.sina.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.lexicon.sina.dto.BookDto;
import se.lexicon.sina.service.BookService;

import java.util.List;

@RestController
public class BookController {

    @Autowired
    private BookService bookService;

    @RequestMapping(value = "/book/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity create(@RequestBody BookDto bookDto) {
        try {
            return new ResponseEntity<>(bookService.create(bookDto) , HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null , HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/book/find/{bookId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity find(@PathVariable("bookId") int bookId) {
        try {
            BookDto bookDto = bookService.findById(bookId);
            if (bookDto != null) {
                return new ResponseEntity<>(bookDto , HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/book/findAllByTitle/{bookTitle}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity findAllByTitle(@PathVariable("bookTitle") String bookTitle) {
        try {
            List<BookDto> bookDtos = bookService.findAllByTitle(bookTitle);
            if (bookDtos.size() != 0) {
                return new ResponseEntity<>(bookDtos , HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/book/findAllByReserved", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity findAllByReserved(@RequestParam("isReserved") boolean isReserved) {
        try {
            List<BookDto> bookDtos = bookService.findAllByReserved(isReserved);
            if (bookDtos != null) {
                return new ResponseEntity<>(bookDtos , HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/book/findAllByAvailable", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity findAllByAvailable(@RequestParam("isAvailable") boolean isAvailable) {
        try {
            List<BookDto> bookDtos = bookService.findAllByAvailable(isAvailable);
            if (bookDtos != null) {
                return new ResponseEntity<>(bookDtos , HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/book/findAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity findAll() {
        try {
            List<BookDto> bookDtos = bookService.findAll();
            if (bookDtos != null) {
                return new ResponseEntity<>(bookDtos , HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/book/delete/{bookId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity delete(@PathVariable("bookId") int bookId) {
        try {
            BookDto bookDto = bookService.findById(bookId);
            if (bookDto != null) {
                bookService.delete(bookId);
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/book/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity update(@RequestBody BookDto bookDto) {
        try {
            BookDto currentBookDto = bookService.findById(bookDto.getBookId());
            if (currentBookDto != null) {
                bookService.update(bookDto);
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
