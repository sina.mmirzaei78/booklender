package se.lexicon.sina.service;

import org.springframework.stereotype.Service;
import se.lexicon.sina.dto.LibraryUserDto;

import java.util.List;

@Service("libraryUserService")
public interface LibraryUserService {
    public abstract LibraryUserDto findById(int userId);
    public abstract LibraryUserDto findByEmail(String email);
    public abstract List<LibraryUserDto> findAll();
    public abstract LibraryUserDto create(LibraryUserDto libraryUserDto);
    public abstract LibraryUserDto update(LibraryUserDto libraryUserDto);
    public abstract boolean delete(int userId);
}