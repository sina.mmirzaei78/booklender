package se.lexicon.sina.service;

import org.springframework.stereotype.Service;
import se.lexicon.sina.dto.LoanDto;

import java.util.List;

@Service
public interface LoanService {
    public abstract LoanDto findById(long loanId);
    public abstract List<LoanDto> findByBookId(int bookId);
    public abstract List<LoanDto> findByUserId(int userId);
    public abstract List<LoanDto> findByisTerminated(boolean isisTerminated);
    public abstract List<LoanDto> findAll();
    public abstract LoanDto create(LoanDto loanDto);
    public abstract LoanDto update(LoanDto loanDto);
}
