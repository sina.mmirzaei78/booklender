package se.lexicon.sina.service;

import org.springframework.stereotype.Service;
import se.lexicon.sina.dto.BookDto;
import java.util.List;

@Service
public interface BookService {
    public  List<BookDto> findAllByReserved(boolean isReserved);
    public  List<BookDto> findAllByAvailable(boolean isAvailable);
    public  List<BookDto> findAllByTitle(String title);
    public  BookDto findById(int bookId);
    public  List<BookDto> findAll();
    public  BookDto create(BookDto bookDto);
    public  BookDto update(BookDto bookDto);
    public  boolean delete(int bookId);
}

