package se.lexicon.sina;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={
        "se.lexicon.sina","se.lexicon.sina"})
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class , args);
    }
}
