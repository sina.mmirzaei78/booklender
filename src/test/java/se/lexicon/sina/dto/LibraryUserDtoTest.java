package se.lexicon.sina.dto;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class LibraryUserDtoTest {
    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(LibraryUserDto.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Test
    public void getLoanDtos() {
    }

    @Test
    public void setLoanDtos() {
    }

    @Test
    public void getUserId() {
    }

    @Test
    public void setUserId() {
    }

    @Test
    public void getRegDate() {
    }

    @Test
    public void setRegDate() {
    }

    @Test
    public void getName() {
    }

    @Test
    public void setName() {
    }

    @Test
    public void getEmail() {
    }

    @Test
    public void setEmail() {
    }
}
