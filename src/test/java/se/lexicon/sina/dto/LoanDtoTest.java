package se.lexicon.sina.dto;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class LoanDtoTest {
    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(LoanDto.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Test
    public void getLoanId() {
    }

    @Test
    public void setLoanId() {
    }

    @Test
    public void getLoanDate() {
    }

    @Test
    public void setLoanDate() {
    }

    @Test
    public void isisTerminated() {
    }

    @Test
    public void setisTerminated() {
    }

    @Test
    public void getBookDto() {
    }

    @Test
    public void setBookDto() {
    }

    @Test
    public void getLibraryUserDto() {
    }

    @Test
    public void setLibraryUserDto() {
    }
}
