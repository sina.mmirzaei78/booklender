package se.lexicon.sina.entity;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class LoanTest {
    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(Loan.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Test
    public void getLoanId() {
    }

    @Test
    public void setLoanId() {
    }

    @Test
    public void getLoanDate() {
    }

    @Test
    public void setLoanDate() {
    }

    @Test
    public void isisTerminated() {
    }

    @Test
    public void setisTerminated() {
    }

    @Test
    public void getBook() {
    }

    @Test
    public void setBook() {
    }

    @Test
    public void getLibraryUser() {
    }

    @Test
    public void setLibraryUser() {
    }

    @Test
    public void testToString() {
    }

    @Test
    public void testEquals() {
    }

    @Test
    public void testHashCode() {
    }
}
