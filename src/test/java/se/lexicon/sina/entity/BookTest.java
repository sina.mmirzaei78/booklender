package se.lexicon.sina.entity;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class BookTest {
    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(Book.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Test
    public void getBookId() {
    }

    @Test
    public void setBookId() {
    }

    @Test
    public void getTitle() {
    }

    @Test
    public void setTitle() {
    }

    @Test
    public void isAvailable() {
    }

    @Test
    public void setAvailable() {
    }

    @Test
    public void isReserved() {
    }

    @Test
    public void setReserved() {
    }

    @Test
    public void getMaxLoanDays() {
    }

    @Test
    public void setMaxLoanDays() {
    }

    @Test
    public void getFinePerDay() {
    }

    @Test
    public void setFinePerDay() {
    }

    @Test
    public void getDescription() {
    }

    @Test
    public void setDescription() {
    }

    @Test
    public void getLoans() {
    }

    @Test
    public void setLoans() {
    }

    @Test
    public void testToString() {
    }

    @Test
    public void testEquals() {
    }

    @Test
    public void testHashCode() {
    }
}
